[![pipeline status](https://gitlab.com/macfol/centogene/badges/master/pipeline.svg)](https://gitlab.com/macfol/centogene/commits/master)
[![coverage report](https://gitlab.com/macfol/centogene/badges/master/coverage.svg)](https://gitlab.com/macfol/centogene/commits/master)



### Requirements

- java 11
- maven 3.3+
- npm 6+
- angular cli 7.3.9+
### Description 

Applications is divided into to modules: 
1. Backend which is based on springboot framework. 
2. Frontend (Web-app) which is based on Angular framework


#### How to build

1. Backend - build jar using maven.
2. Frontend - download dependencies using npm

#### How to start

1. Backend - simply ran the jar created by maven
2. Frontend - run ng cli command 'ng serve'

#### Features

Api documentation is accessible via below link:
http://localhost:8080/swagger-ui.html

UI can be accessed on page "http://localhost:4200"

### Docker

How to build and run a docker image:
1. Build backend using maven
2. build frontend using command "ng build --prod"
3. From main directory run command "docker-compose up -d"