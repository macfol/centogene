import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

const APIEndpoint = environment.baseUrl;

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  failureMessage: string;
  username: string;
  password: string;

  constructor(private router: Router, private http: HttpClient) {
  }

  ngOnInit(): void {
  }

  onRegister(): void {
    if (this.validationPassed()) {
      this.register();
    }
  }

  validationPassed(): boolean {
    if (!this.username || !this.password) {
      this.failureMessage = 'All fields must be provided';
      return false;
    }
    if (this.password.length < 8) {
      this.failureMessage = 'Password must be min. 8 characters long.';
      return false;
    }
    return true;
  }

  register(): void {
    this.callBackend().subscribe(() => {
        this.router.navigate(['/login'], {queryParams: {registry: 'true'}});
      },
      error => {
        this.processFailedAttempt(error);
      });
  }


  private processFailedAttempt(response: any): void {
    const message: string = response.error.message;
    if (message.includes('constraint')) {
      this.failureMessage = 'This username is already taken';
    } else {
      this.failureMessage = message;
    }
  }

  private callBackend(): Observable<void> {
    return this.http.post<void>(`${APIEndpoint}/register`, {
      name: this.username,
      password: this.password
    });
  }

  onClear(): void {
    this.failureMessage = '';
    this.username = '';
    this.password = '';
  }
}
