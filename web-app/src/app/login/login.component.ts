import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../security/authentication.service';
import {AuthenticationStatus} from '../security/authentication-status';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  authenticationStatus: AuthenticationStatus = new AuthenticationStatus();
  username: string;
  password: string;
  message: string;

  constructor(router: Router, private authenticationService: AuthenticationService, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams
      .subscribe(params => {
        this.checkParams(params);
      });
  }

  private checkParams(params): void {
    if (params.hasOwnProperty('returnUrl')) {
      this.authenticationStatus.authenticated = false;
      this.authenticationStatus.reason = 'Needs Authentications';
    } else if (params.hasOwnProperty('registry')) {
      this.message = 'Registered successfully';
    } else if (params.hasOwnProperty('signout')) {
      this.message = 'Signed out';
    }
  }

  onLogin(): void {
    if (!this.username || !this.password) {
      this.authenticationStatus.reason = 'Fields cannot be empty';
      this.authenticationStatus.authenticated = false;
      return;
    }
    this.message = '';
    this.authenticationStatus = this.authenticationService.authenticate(this.username, this.password);
  }

}
