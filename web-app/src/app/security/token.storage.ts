import { Injectable } from '@angular/core';
import { Token } from './token';


const TOKEN_KEY = 'authorization';

@Injectable()
export class TokenStorage {

    constructor() { }

    signOut() {
        window.sessionStorage.removeItem(TOKEN_KEY);
        window.sessionStorage.clear();
    }

    public saveToken(token: Token) {
        window.sessionStorage.removeItem(TOKEN_KEY);
        window.sessionStorage.setItem(TOKEN_KEY, JSON.stringify(token));
    }

    public getToken(): Token {
        return JSON.parse(sessionStorage.getItem(TOKEN_KEY));
    }

}
