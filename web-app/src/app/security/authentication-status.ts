export class AuthenticationStatus {
  authenticated?: boolean;
  reason?: string;
}
