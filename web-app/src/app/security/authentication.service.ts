import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthenticationStatus} from './authentication-status';
import {Token} from './token';
import {TokenStorage} from './token.storage';
import {environment} from '../../environments/environment';
import {tap} from "rxjs/operators";

const BASE_URL = environment.baseUrl;

@Injectable()
export class AuthenticationService {

  constructor(private http: HttpClient, private router: Router, private tokenStorage: TokenStorage) {
  }

  authenticate(username: string, password: string): AuthenticationStatus {
    const authStat: AuthenticationStatus = new AuthenticationStatus();

    this.fetchToken(username, password).subscribe(
      (response: HttpResponse<any>) => {
        this.processSuccessfulAttempt(response, username);
      },
      error => {
        this.processFailedAttempt(authStat, error);
      }
    );
    return authStat;
  }

  private processSuccessfulAttempt(response: HttpResponse<any>, username: string): void {
    const jwt = response.headers.get("authorization");
    const token: Token = new Token(username, jwt);
    this.tokenStorage.saveToken(token);
    this.router.navigate(['/home']);
  }

  private processFailedAttempt(authStat: AuthenticationStatus, error: any): void {
    authStat.authenticated = false;
    if (error.status === 403) {
      authStat.reason = 'Wrong credentials';
    } else {
      authStat.reason = 'Server down';
    }
  }

  signout() {
    this.tokenStorage.signOut();
    this.router.navigate(['']);
  }

  private fetchToken(username: string, password: string): Observable<HttpResponse<any>> {
    const credentials = {name: username, password: password};
    const contentHeader = new HttpHeaders({"Content-Type": "application/json"});
    return this.http.post<HttpResponse<any>>(`${BASE_URL}/login`, credentials, {
      headers: contentHeader,
      observe: 'response'
    });
  }
}
