export class Token {
    name: string;
    jwt: string;

    constructor(name: string, jwt: string) {
        this.name = name;
        this.jwt = jwt;
    }
}
