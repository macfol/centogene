package com.app.centogene.feature.account.service;

import com.app.centogene.configuration.ServiceTest;
import com.app.centogene.configuration.TestDbInit;
import com.app.centogene.feature.account.model.Account;
import com.app.centogene.feature.account.dto.AccountDto;
import com.app.centogene.feature.account.dto.RegistrationDto;
import com.app.centogene.feature.account.repository.AccountRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@Transactional
public class AccountServiceTest extends ServiceTest {

    @Autowired
    AccountRepository accountRepository;
    @Autowired
    AccountService accountService;

    @Test
    public void shouldGetListOfAllAccounts() {
        //Given, When
        final List<AccountDto> accounts = accountService.getList();

        //Then
        assertEquals(TestDbInit.NUMBER_OF_USERS, accounts.size());
    }

    @Test
    public void shouldRegisterNewAccount() {
        //Given
        final RegistrationDto registrationDto = new RegistrationDto("test", "dummy");
        //When
        accountService.registerUser(registrationDto);

        //Then
        final List<AccountDto> accounts = accountService.getList();
        assertEquals(TestDbInit.NUMBER_OF_USERS + 1, accounts.size());

        final Optional<Account> newAccount = accountRepository.findByName(registrationDto.getName());
        assertTrue(newAccount.isPresent());
        assertNotEquals(registrationDto.getPassword(), newAccount.get().getPassword());
    }

}