package com.app.centogene.configuration;

import com.app.centogene.feature.account.model.Account;
import com.app.centogene.feature.account.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
@RequiredArgsConstructor
@Slf4j
public class TestDbInit {

    public static final int NUMBER_OF_USERS = 5;

    private final PasswordEncoder encoder;
    private final AccountRepository accountRepository;


    @PostConstruct
    public void initData() {
        log.info("Initializing test data");
        initUsers();
    }

    private void initUsers() {
        final List<Account> accounts = IntStream.range(0, NUMBER_OF_USERS)
                .mapToObj(iteration -> Account.builder()
                        .name("username" + iteration)
                        .password(encoder.encode("password"))
                        .build())
                .collect(Collectors.toList());

        accountRepository.saveAll(accounts);
    }
}
