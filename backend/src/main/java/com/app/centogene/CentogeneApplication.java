package com.app.centogene;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CentogeneApplication {

	public static void main(String[] args) {
		SpringApplication.run(CentogeneApplication.class, args);
	}

}
