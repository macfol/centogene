package com.app.centogene.configuration.security.service;

import com.app.centogene.feature.account.model.Account;
import com.app.centogene.feature.account.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static java.util.Collections.emptyList;

@Service
@RequiredArgsConstructor

public class UserDatabaseDetailsService implements UserDetailsService {

    private final AccountRepository accountRepository;

    @Override
    public UserDetails loadUserByUsername(final String username) {
        final Account applicationUser = accountRepository.findByName(username).orElseThrow(() -> new UsernameNotFoundException(username));
        return new User(applicationUser.getName(), applicationUser.getPassword(), emptyList());
    }

}
