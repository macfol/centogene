package com.app.centogene.configuration.security.exception;

import com.app.centogene.configuration.CentogeneException;

public class AuthException extends CentogeneException {

  public AuthException(final String message) {
    super(message);
  }
}
