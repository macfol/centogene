package com.app.centogene.configuration;

public class CentogeneException extends RuntimeException {

    public CentogeneException(final String message) {
        super(message);
    }

}
