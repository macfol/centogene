package com.app.centogene.feature.account.controller;

import com.app.centogene.feature.account.dto.AccountDto;
import com.app.centogene.feature.account.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("accounts")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @GetMapping
    public List<AccountDto> accountList() {
        return accountService.getList();
    }
}


