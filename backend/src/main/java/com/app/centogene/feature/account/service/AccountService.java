package com.app.centogene.feature.account.service;

import com.app.centogene.feature.account.model.Account;
import com.app.centogene.feature.account.dto.AccountDto;
import com.app.centogene.feature.account.dto.RegistrationDto;
import com.app.centogene.feature.account.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
@RequiredArgsConstructor
public class AccountService {

    private final PasswordEncoder encoder;
    private final AccountRepository accountRepository;

    public List<AccountDto> getList() {
        return accountRepository.findAll().stream()
                .map(account -> new AccountDto(account.getId(), account.getName()))
                .collect(Collectors.toList());
    }

    public void registerUser(final RegistrationDto registrationDto) {
        final Account accountToRegister = Account.builder()
                .name(registrationDto.getName())
                .password(encoder.encode(registrationDto.getPassword()))
                .build();

        accountRepository.save(accountToRegister);
    }
}
