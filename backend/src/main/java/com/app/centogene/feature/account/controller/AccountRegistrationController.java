package com.app.centogene.feature.account.controller;


import com.app.centogene.feature.account.dto.RegistrationDto;
import com.app.centogene.feature.account.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.app.centogene.configuration.security.constants.SecurityConstants.SIGN_UP_URL;

@RestController
@RequestMapping(SIGN_UP_URL)
@RequiredArgsConstructor
public class AccountRegistrationController {

    private final AccountService accountService;

    @PostMapping
    public void registerAccount(@RequestBody final RegistrationDto registrationDto) {
        accountService.registerUser(registrationDto);
    }
}
